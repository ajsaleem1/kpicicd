trigger HolidayNotificationTrigger on Holiday_Calendar__c (after insert, after update, after delete) {
    if(trigger.isAfter){
        if(trigger.isInsert){
            HolidayNotification.insertHoliday(trigger.new);
        }
        if(trigger.isUpdate){
            HolidayNotification.updateHoliday(trigger.new);
        }
        if(trigger.isDelete){
            HolidayNotification.deleteHoliday(trigger.old);
        }
    }
    
}