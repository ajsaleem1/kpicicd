trigger trgr_CheckDupeAccounts on Account (before Insert, before Update) {
    Set<String> accountNames = new Set<String>();
    for(Account acc : trigger.new)
        accountNames.add(acc.Name);
    
    List<Account> matchingAccounts = 
        [
            SELECT
                Id,
                Name
            FROM
                Account
            WHERE
                Name IN: accountNames
        ];
    Map<String,Id> matchingAccountsMap = new Map<String,Id>();
    for(Account acc : matchingAccounts)
        matchingAccountsMap.put(acc.Name, acc.Id);
    
    for(Account acc : trigger.new){
        if(matchingAccountsMap.containsKey(acc.Name))
        {
            String message = 
                    'There is a Duplicate Account with the same Name at: <a href="' + 
                    Url.getSalesforceBaseUrl().toExternalForm() + '/' +
                    matchingAccountsMap.get(acc.Name) + '">' +
                    acc.Name + '</a>';

            if(trigger.isInsert){
                acc.addError(
                    message,
                    false
                );
            }
            else if(            
                trigger.isUpdate &&
                trigger.oldMap.get(acc.Id).Name != acc.Name
            ){
                acc.addError(
                    message,
                    false
                );    
            }
        }
 
    

}
}