trigger AutoFillEmployeeUtilizationTrigger on KPI_Employee_Goal__c (after insert, after update) {
    for(KPI_Employee_Goal__c EG : Trigger.new){
        If(EG.Status__c=='Final'){
            AutoFillEmployeeUtilizationClass.autoFill(EG.name,EG.Employee_Utilization_Start_Date__c, EG.Employee_Name__c, EG.Employee_Location__c, EG.Target_Billable_Utilization__c);
        }
    }

}