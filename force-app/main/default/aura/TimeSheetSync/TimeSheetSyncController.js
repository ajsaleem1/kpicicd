({
	"echo" : function(component, event, helper) {
        var action = component.get("c.fillTimeSheet");
        var startDate = component.find('startDate').get('v.value');
        var endDate = component.find('endDate').get('v.value');
        console.log(startDate);
        
        if(startDate > endDate || startDate == "" || endDate == "")
            alert('Not valid entries');
        else{
            
             action.setParams({updatedAfter : component.find('endDate').get('v.value'), startDate : component.find('startDate').get('v.value')});
        
        
        action.setCallback(this, function(response){
                           
          var state = response.getState();
         if(state === "SUCCESS"){
            alert("SYNC Started");
         } else {
             console.log("failed");
         }
        });
    
    	$A.enqueueAction(action);
		
        }
            
	}
})