public class TimeSheetFiller {
    
    //extract data from avaza through web service and fill over of details in timesheet object through dml statement
    @AuraEnabled
   // @future(callout = true)
    public static void fillTimeSheet(String updatedAfter, String startDate){
        
         List<Employee__c> employees = [select id, email__c, Name, Last_Name__c from employee__c where status__c = 'active'];
         for(Employee__c values : employees) {
             System.debug('Fetching Timesheet Data for Employee ' + values.email__c );
             try{
                 TimesheetFillerHelper.extractDataAndFillTimeSheetList
                    (values.email__c, values.Name, values.Last_Name__c, values.id, startDate, updatedAfter);
             }catch(Exception e){
                 System.debug(e.getCause());
            }
        }
        
        sumTimeSheetRecords();
    }
    
    private static void sumTimeSheetRecords(){
        
        Map<String, Decimal> rollUpSummary = new Map<String, Decimal>();
        AggregateResult[] groupResult = [select employee__c, timesheet_month__c, timesheet_year__c, sum(duration__c)sume from timesheet__c group by employee__c, timesheet_month__c, timesheet_year__c];
        System.debug(groupResult);
        
        for(AggregateResult ar : groupResult){
            rollUpSummary.put((string) ar.get('employee__c') + (string) ar.get('timesheet_month__c') + (string) ar.get('timesheet_year__c'), Integer.valueOf(ar.get('sume')));
            
        }
        List<KPI_Employee_Target_utilization__c> updateBillableHours = new List<KPI_Employee_Target_utilization__c>();
        for(KPI_Employee_Target_Utilization__c var : [select employee_name__r.id, month__c, year__c from KPI_Employee_Target_Utilization__c ]){
            
            String theKey = String.valueOf(var.employee_name__r.id) + String.valueOf(var.month__c) + String.valueOf(var.year__c);
            if(rollUpSummary.get(theKey) != null){
                var.Total_Billable_Hours__c = rollUpSummary.get(theKey);
                updateBillableHours.add(var);
            }
        }
        
        if(updateBillableHours.size() > 0)
            upsert updateBillableHours;
        
    }
   
}