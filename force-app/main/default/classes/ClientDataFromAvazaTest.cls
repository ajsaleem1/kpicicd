@istest
public class ClientDataFromAvazaTest {
    @istest
    static void mockTest()
    {  
        Account acc = new Account();
        acc.Avaza_Company_ID__c=569;
        acc.Name='Android';
        acc.BillingStreet='3-2-56/45,Vijaynagar';
        acc.BillingCity='Hyderabad';
        acc.BillingState='Telengana';
        acc.BillingCountry='India';
        acc.BillingPostalCode='IND';
        acc.Fax='4343433456';
        acc.Phone='6767898990';
        acc.Website='www.android.com';
        insert acc;
       
        Test.setMock(HttpCalloutMock.class, new ClientDataFromAvazaMock());
       
        Test.startTest();
        ClientAvazaSchedular cli = new ClientAvazaSchedular();
        String sch='0 26 19 7 5 ?';
        String jobId =system.schedule('Test16',sch,cli);
        Test.stopTest();
          
       
    }
  
}