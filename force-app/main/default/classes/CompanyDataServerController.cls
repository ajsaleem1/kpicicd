public class CompanyDataServerController {
    @AuraEnabled
	public static List<Account> getData()
    {
        ClientDataFromAvaza.getData();
        return [Select Name,BillingAddress,Phone from Account];
    }
}