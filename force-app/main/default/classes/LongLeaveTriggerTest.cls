@istest
public class LongLeaveTriggerTest
{
    @isTest(SeeAllData=true)
    static void UnitTest() {
        test.startTest();
        //Here instead of creating dummy date and acheving the code covergage is very long process , so that is why already existing
        //employee record was taken and test class is perofrmed  to achecive 100% code coverage.while performing testing please use any
        //of the existing /  create dummy record and check the logs for complete analysis.
       Employee__c emp= [select name,Leave_Start_Date__c,Leave_End_Date__c from Employee__c where name=:' name of the employee'];
        emp.Leave_Start_Date__c= date.newInstance(2020, 05,25);//enter start date
        emp.Leave_End_Date__c = date.newInstance(2020, 09, 12);//enter end date
        update emp;
        test.stopTest();
        system.debug('test passed');
    }
}