@isTest
public class TimeSheetFillersTest {
    
    @isTest
    static void testfillTimesheet(){
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetTimeSheetResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.setMock(HttpCalloutMock.class, mock);
        employee__c testVal = new employee__c();
        testVal.name = 'siddhart';
        testVal.Last_Name__c = 'abhi';
        testVal.Email__c = 'jeevat@sidgs.com';
        testVal.Phone__c = '1234567890';
        testVal.Location__c = 'United States';
        testVal.Status__c = 'Active';
        insert testVal;
        kpi_employee_goal__c goal = new  kpi_employee_goal__c();
        goal.Employee_Name__c = testVal.Id;
        goal.Employee_Utilization_Start_Date__c = Date.newInstance(2020, 04, 01);
        goal.Target_Billable_Utilization__c = 80;
        goal.Status__c = 'final';
        Test.startTest();
        try{
             	TimeSheetFiller.fillTimeSheet('04/30/2020', '04/01/2020');
        } catch(Exception e){
            System.assert(false);
        }
       	Test.stopTest();
        Timesheet__c result = [select duration__c from timesheet__c where entry_date__c = :Date.newInstance(2020, 04, 24) limit 1];
        System.assert(result != null);
        insert goal;
         try{
             	TimeSheetFiller.fillTimeSheet('04/30/2020', '04/01/2020');
        } catch(Exception e){
            System.assert(false);
        }
          //Test.stopTest();
        KPI_Employee_Target_utilization__c var = [select total_billable_hours__c from kpi_employee_target_utilization__c where month__c = '4' limit 1];
        //System.debug(var.Employee_Name__c);
        System.assertEquals(8.0, var.Total_Billable_Hours__c);
        
      
      
    }

}