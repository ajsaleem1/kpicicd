//This schedulable apex class will update the Total Billable hours field in TargetUtilization Obj Every Month at specified time
public class Kpi_Update_Total_Billable_Hours implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        List<Kpi_Employee_Target_Utilization__c> updList=new List<Kpi_Employee_Target_Utilization__c>();
        
        
        List<Employee__c> empList=[Select id,Billable_Hours__c from Employee__c];
        
        List<Kpi_Employee_Target_Utilization__c> utilList=[select id,Total_Billable_Hours__c,Month__c,Employee_Name__c,year__c from Kpi_Employee_Target_Utilization__c];
        
        for(Employee__c emp:empList)
        {
        for(Kpi_Employee_Target_Utilization__c util:utilList)
        {
            if(util.Employee_Name__c==emp.id && util.Month__c=='January' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
                {
                    util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                    updList.add(util);
                }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='February' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='March' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='April' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='May' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='June' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
             else if(util.Employee_Name__c==emp.id && util.Month__c=='July' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='August' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='September' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='October' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='November' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else if(util.Employee_Name__c==emp.id && util.Month__c=='December' && util.year__c=='2020' && util.Total_Billable_Hours__c==null)
            {
                util.Total_Billable_Hours__c=emp.Billable_Hours__c;
                updList.add(util);
            }
            else
            {
             System.debug('There are no records to update');
            }
        } 
        }
        update updList;        
    }
}