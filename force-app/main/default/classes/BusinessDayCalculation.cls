public class BusinessDayCalculation  
{  
    public static Integer calculateWorkingDays(Date startDate, Date endDate,String Location)  
    {          
       	set<Date> holidaysSet = new set<Date>();  
        for(Holiday_Calendar__c currHoliday : [Select Holiday_Date__c from Holiday_Calendar__c where Location__c includes (:Location)])  
        {  
            holidaysSet.add(currHoliday.Holiday_Date__c);  
        }  
         
        Integer workingDays = 0;  
         
        for(integer i=0; i <= startDate.daysBetween(endDate); i++)  
        {  
            Date dt = startDate + i;  
            DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());  
            String todayDay = currDate.format('EEEE');  
            if(todayDay != 'Saturday' && todayDay !='Sunday' && (!holidaysSet.contains(dt)))  
                {  
                    workingDays = workingDays + 1;  
                }     
               
        }  
        return workingDays;  
    }  
}