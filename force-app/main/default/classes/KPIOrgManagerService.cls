public with sharing class KPIOrgManagerService {
    public KPIOrgManagerService() {

    }

    @future
    public static void clearAllOrgData() {

        List<TimeSheet__c> timesheets = [select ID from TimeSheet__c ];
        delete timesheets;

        List<Account> accounts = [select ID from Account] ; 
        delete accounts;

        List<Projects__c> projects = [select ID from Projects__c];
        delete projects ; 

        
    }

}
