public class WorkingDayCal {
 public static void WorkingDayCalculation(List<Working_Day__c> works)
     {
          String Location;
          Date thisdate;
    
       List<Working_Day__c> w=new List<Working_Day__c>();
       Integer count=[SELECT COUNT() from Working_Day__c];
     for(Working_Day__c wd:works)
     {
       thisdate=wd.Date__c;
       Location=wd.Location__c;
    }
    if(count==1)
    {
        for(Integer i=thisdate.month();i<=12;i++)
       {
             Integer days;
      		 Working_Day__c work=new Working_Day__c();
      		 work.Date__c=Date.newInstance(thisdate.year(),i,1);
      		 work.Location__c='India';
             days=date.daysInMonth(thisdate.year(),i);
             work.Total_Working_Days__c=BusinessDayCalculation.calculateWorkingDays(Date.newInstance(thisdate.year(),i,1),Date.newInstance(thisdate.year(),i,days),'India');
              w.add(work);
       }
       for(Integer i=thisdate.month();i<=12;i++)
       {
             Integer days;
      		 Working_Day__c work=new Working_Day__c();
      		 work.Date__c=Date.newInstance(thisdate.year(),i,1);
      		 work.Location__c='Canada';
              days=date.daysInMonth(thisdate.year(),i);
             work.Total_Working_Days__c=BusinessDayCalculation.calculateWorkingDays(Date.newInstance(thisdate.year(),i,1),Date.newInstance(thisdate.year(),i,days),'Canada');
      		 w.add(work);
       }
       for(Integer i=thisdate.month();i<=12;i++)
       {
             Integer days;
      		 Working_Day__c work=new Working_Day__c();
      		 work.Date__c=Date.newInstance(thisdate.year(),i,1);
      		 work.Location__c='Singapore';
           days=date.daysInMonth(thisdate.year(),i);
             work.Total_Working_Days__c=BusinessDayCalculation.calculateWorkingDays(Date.newInstance(thisdate.year(),i,1),Date.newInstance(thisdate.year(),i,days),'Singapore');
      		 w.add(work);
       }
       for(Integer i=thisdate.month();i<=12;i++)
       {
             Integer days;
      		 Working_Day__c work=new Working_Day__c();
      		 work.Date__c=Date.newInstance(thisdate.year(),i,1);
      		 work.Location__c='United States';
           days=date.daysInMonth(thisdate.year(),i);
             work.Total_Working_Days__c=BusinessDayCalculation.calculateWorkingDays(Date.newInstance(thisdate.year(),i,1),Date.newInstance(thisdate.year(),i,days),'United States');
      		 w.add(work);
       }
  		insert w;
       }
       
     }
}