public class OpportunityTriggerHandler 
{
	public static void beforeInsertLogic(List<Opportunity> newOpp, Boolean isBefore, Boolean isInsert)
    {
        System.debug('newOpp---'+newOpp + 'isBefore--'+isBefore + 'isInsert-'+isInsert);
        for(Opportunity opp: newOpp)
        {
            if(opp.stagename == 'Closed Won')
            {
                opp.addError('While Inserting Stage Can Not be Closed Won');
            }
        }
    }
}