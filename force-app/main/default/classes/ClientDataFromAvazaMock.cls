@isTest
global class ClientDataFromAvazaMock implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest request)
    {
          System.assertEquals('https://api.avaza.com/api/Company', request.getEndpoint());
         System.assertEquals('GET', request.getMethod());
        
       
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type','application/json');
        response.setBody('{"Companies":[{"CompanyID":567,"CompanyName":"OnePlus","BillingAddressLine":"3-2-56/45,Malakpet","BillingAddressCity":"Hyderabad","BillingAddressState":"Telengana","BillingAddressPostCode":"400026","BillingCountryCode":"IN","Phone":"4545676789","Fax":"4545676789","website":"www.one+.com"},{"CompanyID":569,"CompanyName":"Android","BillingAddressLine":"3-2-56/45,Malakpet","BillingAddressCity":"Hyderabad","BillingAddressState":"Telengana","BillingAddressPostCode":"400026","BillingCountryCode":"IN","Phone":"4545676789","Fax":"4545676789","website":"www.one+.com"}]}');
           response.setStatusCode(200);
        return response;
    }
}