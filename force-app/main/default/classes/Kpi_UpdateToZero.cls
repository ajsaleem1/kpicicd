//This will update billble hours field to 0 in Employee Obj at every month at a particular date & time using Schedulable
public class Kpi_UpdateToZero implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        List<Employee__c> empList=[Select id,Billable_Hours__c	from Employee__c];
        
        for(Employee__c e:empList)
        {
            e.Billable_Hours__c=0;
        }
        update empList;
    }
    
    

}