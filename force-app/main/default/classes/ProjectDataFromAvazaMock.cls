@isTest
global class ProjectDataFromAvazaMock implements HttpCalloutMock {
global HttpResponse respond(HttpRequest request)
    {
  System.assertEquals('https://api.avaza.com/api/Project', request.getEndpoint());
      System.assertEquals('GET', request.getMethod());


        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type','application/json');
        response.setBody('{"Projects": [{"ProjectID": 3456,"Title": "Contact"},{"ProjectID": 3455,"Title": "Application"}]}');
    response.setStatusCode(200);
        return response;
    }
}