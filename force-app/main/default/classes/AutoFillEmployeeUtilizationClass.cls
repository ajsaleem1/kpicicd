public class AutoFillEmployeeUtilizationClass {
    public static void autoFill(String goalName,Date employeeUtilizationStartDate, ID employeeName, String employeeLocation,Decimal employeeTargetUtilization){
       
        List<KPI_Employee_Target_Utilization__c> ETU = new List<KPI_Employee_Target_Utilization__c>();
        Map<string,KPI_Employee_Target_Utilization__c> mapETU = new Map<string,KPI_Employee_Target_Utilization__c>();
        for(KPI_Employee_Target_Utilization__c data: [select utilizationID__c, Target_Utilization_Percentage__c from KPI_Employee_Target_Utilization__c]){
            mapETU.put(data.utilizationID__c, data);
        }
        System.debug('mapEtu -------> ' + mapETU);
        Date startDate = employeeUtilizationStartDate; 
        Date endDate = Date.newInstance(employeeUtilizationStartDate.year(), 12, 31);
        date startOfMonth = startDate.toStartOfMonth();
       	date endOfMonth = startofMonth.addDays(date.daysInMonth(startDate.year(), startDate.month())-1);
        
        for(date d=startDate; d <= endDate; d= d.addMonths(1)){
            
            
            String mapid = goalName + string.valueof(d.toStartOfMonth())+string.valueOf(d.year());
            system.debug(mapid);
             if(mapETU.containsKey(mapid)){
                 KPI_Employee_Target_Utilization__c temp = mapETU.get(mapid);
                 system.debug(temp + '   ' + employeeTargetUtilization);
                 temp.Target_Utilization_Percentage__c = employeeTargetUtilization;
                 ETU.add(temp);
                 
             }else {
            KPI_Employee_Target_Utilization__c newEmpTargetUtilization = new KPI_Employee_Target_Utilization__c();
            newEmpTargetUtilization.UtilizationID__c =  mapid;   
            newEmpTargetUtilization.Employee_Name__c = employeeName;
            newEmpTargetUtilization.Target_Utilization_Percentage__c = employeeTargetUtilization;
            newEmpTargetUtilization.Date__c= d;
            Integer workingDays = BusinessDayCalculation.calculateWorkingDays(startOfMonth,endOfMonth,employeeLocation);
            newEmpTargetUtilization.Payroll_Days__c = workingDays;
            ETU.add(newEmpTargetUtilization);  
             }
                     
        }
        try{
          upsert ETU; 
      }catch(exception e){
          system.debug(etu);
          system.debug(e.getMessage() +  'AutoFillClass');
      }
    }
}