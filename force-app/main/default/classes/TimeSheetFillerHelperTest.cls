@isTest
public class TimeSheetFillerHelperTest {
	
    @isTest
    static void testExtractDataAndFillTimeSheetList(){
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetTimeSheetResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.setMock(HttpCalloutMock.class, mock);
        employee__c testVal = new employee__c();
        testVal.name = 'siddhart';
        testVal.Last_Name__c = 'abhi';
        testVal.Email__c = 'jeevat@sidgs.com';
        testVal.Phone__c = '1234567890';
        testVal.Location__c = 'United States';
        testVal.Status__c = 'Active';
        insert testVal;
        Test.startTest();
        TimeSheetFillerHelper.extractDataAndFillTimeSheetList(testVal.Email__c, testVal.Name, testVal.Last_Name__c, testVal.Id, '04/01/2020', '04/30/2020');
        Test.stopTest();
        Timesheet__c retData = [select email__c from timesheet__c where email__c = 'jeevat@sidgs.com' limit 1];
        System.assertEquals(testVal.email__c, retData.Email__c);
        
    }
}