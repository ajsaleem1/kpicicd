<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Name_Copy</fullName>
        <description>Duplicates the Account name into the Account Name copy field.</description>
        <field>Name</field>
        <formula>Account_Name_Copy__c</formula>
        <name>Account Name Copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Name Copy</fullName>
        <actions>
            <name>Account_Name_Copy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Name    =   Account_Name_Copy__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
