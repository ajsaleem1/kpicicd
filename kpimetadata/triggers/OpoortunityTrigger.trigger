trigger OpoortunityTrigger on Opportunity (before insert, After Insert, Before Update, After Update, 
                                           Before Delete, After Delete) 
{
	OpportunityTriggerHandler.beforeInsertLogic(Trigger.new, Trigger.isBefore, Trigger.isInsert);
}