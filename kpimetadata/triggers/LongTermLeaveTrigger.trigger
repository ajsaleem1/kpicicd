trigger LongTermLeaveTrigger on Employee__c (after insert,after update) {
    for(Employee__c emp : trigger.new){
        if(emp.Leave_Start_Date__c != null && emp.Leave_End_Date__c != null && emp.Leave_Start_Date__c.daysBetween(emp.Leave_End_Date__c )>61 ){
            integer lsd=emp.Leave_Start_Date__c.month();
            integer led=emp.Leave_End_Date__c.month();
            List <integer> lst = new list<integer>();
            for(integer i=lsd+1;i<led;i++){
                lst.add(i);
            }
            list<KPI_Employee_Target_Utilization__c> etu= [select Target_Utilization_Percentage__c,Month__c,Employee_Name__c 
                                                           FROM KPI_Employee_Target_Utilization__c 
                                                           where Employee_Name__c = :emp.id]; 
            for(integer i1:lst){
                for(KPI_Employee_Target_Utilization__c ketu:etu){
                    if(i1==integer.valueOf(ketu.Month__c)){
                        ketu.Target_Utilization_Percentage__c=0;
                    }
                    
                }
                
            }
            update etu;
        }
    }
}