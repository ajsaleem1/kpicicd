trigger CalculatingWorkingDaysTrigger on Employee_PayRoll__c (before insert, before update) {
    for(Employee_PayRoll__c EP : Trigger.New){    
        Integer workingDays = BusinessDayCalculation.calculateWorkingDays(EP.First_Month__c,EP.Last_Month__c,EP.Employee_Name__r.Location__c);
        EP.Payroll_Days__c = workingDays;
    }
}