({
	sync : function(component, event, helper) {
        component.set("v.visibility",true);
        component.set("v.isOpen", true);
	},
    yes :function(component, event, helper) {
        component.set("v.isOpen", false);
		/*component.set("v.columns",[
            {label:'Company Name',fieldName:'Name',type:'text'}   
            
        ]);*/
        var action=component.get("c.getData");
        action.setCallback(this,function(response){
            var state=response.getState();
            console.log(state);
            if(state==='SUCCESS')
            {
                var data = response.getReturnValue();
                //component.set('v.data',data);
                component.set("v.visibility",false);
                component.find('notifLib').showToast({
            		"title": "Success!",
            		"message": "Sync process completed successfully.",
                    "variant":"success"
       			 });
            }
            else
            {
                component.find('notifLib').showToast({
            		"title": "Failed!",
            		"message": "Sync process Failed.",
                    "variant":"error"
                });
            }
        });
        $A.enqueueAction(action);
	},
    no:function(component, event, helper) {  
      component.set("v.isOpen", false);
      component.set("v.visibility",false);
    },
    closeModel : function(component, event, helper) { 
      component.set("v.isOpen", false);
      component.set("v.visibility",false);
      
    }
    
})