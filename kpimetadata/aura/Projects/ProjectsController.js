({
sync : function(component, event, helper) {
        component.set("v.visibility",true);
        component.set("v.isOpen", true);
},
    yes:function(component, event, helper) {
        component.set("v.isOpen", false);
        console.log('Example');
component.set("v.columns",[
            {label:'Title',fieldName:'Name',type:'text'},  
            {label:'ProjectID',fieldName:'Avaza_Project_ID__c',type:'number'},
            
        ]);
        console.log('Example');
        var action=component.get("c.getProjectDataFromAvaza");
        action.setCallback(this,function(response){
            var state=response.getState();
            console.log(state);
            if(state==='SUCCESS')
            {
                var data = response.getReturnValue();
                console.log(data);
                component.set('v.data',data);
    			component.set("v.visibility",false);
    component.find('notifLib').showToast({
                    "title": "Success!",
                    "message": "The Sync has been completed successfully .",
                    "variant":"success"
        });
            }
        });
        $A.enqueueAction(action);
},
    no:function(component, event, helper) {
      component.set("v.isOpen", false);
      component.set("v.visibility",false);
    },
    closeModel:function(component,event,helper){
    component.set("v.isOpen", false);
      component.set("v.visibility",false);
    }
})