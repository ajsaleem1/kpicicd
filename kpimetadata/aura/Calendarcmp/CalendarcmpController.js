({
    Submit : function(component, event, helper) {
        var year,month,location;
        month=component.get("v.month");
        year=component.get("v.year");
        location=component.get("v.location");
        var action = component.get("c.getMethod");
        console.log(action);
      action.setParams({
         "year": year,
         "month": month, 
          "location":location
      });  
      action.setCallback(this, function(response) {
          var state=response.getState();
          console.log(state);
          if(state==='SUCCESS')
          {
              var result =response.getReturnValue();
               console.log(""+result);
              component.set("v.holidays",result[0]);
              component.set("v.workingDays",result[1]);
              component.set("v.display",true);
          }
      });
      $A.enqueueAction(action);
    }
})