public class TimeSheetFillerHelper {
    
    
    public class RecordNotFoundInAvazaException extends Exception{}
    
    //webservice call to avaza, fetch data from avaza, and insertion of data to the timesheet object in salesforce
    
    public static void extractDataAndFillTimeSheetList(String email, String FirstName, String LastName, ID employee, String startDate, String updatedAfter){
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();    
        //Date lastMonthDate = System.today().addMonths(-1);
        System.debug('https://api.avaza.com/api/timesheet?pagesize=1000&updatedAfter=' 
            + updatedAfter + '&userEmail=' + email +'&isBillable=' + true);

        request.setEndpoint
            ('https://api.avaza.com/api/timesheet?pagesize=1000&userEmail=' + email +'&isBillable=' + true +'&EntryDateFrom=' + startDate + '&EntryDateTo=' + updatedAfter);
        request.setMethod('GET');
        request.setHeader('Authorization', 'Bearer ' + String.valueOf(System.Label.Avaza_Bearer_Token));
        request.setHeader('Content-Type','application/json');
        HttpResponse response = http.send(request);//web service call to avaza
        List<TimeSheet__c> timeSheetRecords = new List<TimeSheet__c>();
        
        //map already existing timesheet data for update operation using timesheetentryid as a key
        Map<Decimal, sObject> timeMap = new Map<Decimal, sObject>();
		for(Timesheet__c variables : [select id, timesheetEntryId__c, duration__c, isBillable__c, entry_Date__c from timesheet__c])
		{
    		timeMap.put(variables.timesheetEntryId__c, variables);
		}
			System.debug('existing data ' + timeMap);
        
        
        if(response.getStatusCode() == 200){
            
             Map<String, Object> totalList = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
             List<Object> timeSheets = (List<Object>) totalList.get('Timesheets');
             System.debug('Incoming data' + totalList);
             
            //put each timesheet record from avaza to the list timesheetrecords
            for(Object var : timesheets){
                
                if(var != null){
                     Map<String, object> timesheetVariables = (Map<String, Object>) var;
                     insertTimeSheettoList(email, timesheetVariables, FirstName, LastName, employee, timesheetRecords, timeMap);                
                	System.debug(timesheetVariables);
                } else { throw new RecordNotFoundInAvazaException(response.getStatus());}
               
            }
                             
        } else {
            System.debug('status= ' + response.getStatus() + ' status code = ' + response.getStatusCode());
           
            
        }  
        
        System.debug('Data to be inserted ' + timesheetRecords );
        
        //insert record into timesheet object in salesforce
        if(timesheetRecords.size() > 0)
            upsert timesheetRecords;
    }
    
    //prepping timesheet object and add it to the list of timesheet records for dml operation.
    private static void insertTimesheetToList(String email, Map<String, Object> timesheetVariables, String FirstName, String LastName, id employee, List<TimeSheet__c> records, map<Decimal, sObject> timeMapData){
        
        if(timeMapData.containsKey((Integer) timesheetVariables.get('TimesheetEntryID'))){
            timeSheet__c record = (timeSheet__c) timeMapData.get((Integer) timesheetVariables.get('TimesheetEntryID'));
            record.isBillable__c = (boolean) timesheetvariables.get('isBillable');
            record.Duration__c = (Decimal) timesheetVariables.get('Duration');
            Records.add(record);
            
        } else{
            
        Timesheet__c timesheetRecord = new TimeSheet__c();
        timeSheetRecord.TimeSheetEntryId__c = (Integer) timesheetVariables.get('TimesheetEntryID');
        timesheetRecord.First_Name__c = FirstName;
        timesheetRecord.Last_Name__c = LastName;
        timesheetRecord.Email__c = email;
        timeSheetRecord.isBillable__c = (boolean) timesheetvariables.get('isBillable');
        timeSheetRecord.Duration__c = (Decimal) timesheetVariables.get('Duration');
  		List<String> dateVar = ((string) timeSheetVariables.get('EntryDate')).split('T')[0].split('-');
        Date Entry_Date = date.newInstance(Integer.valueOf(dateVar[0]), Integer.valueOf(dateVar[1]), Integer.valueOf(dateVar[2]));
        timeSheetRecord.Entry_Date__c = Entry_Date;    
        timeSheetRecord.employee__c = employee;
        timeSheetRecord.timesheet_month__c = String.valueOf(Entry_Date.month());
        timeSheetRecord.Timesheet_Year__c = String.valueOf(Entry_Date.year());    
        Records.add(timesheetRecord);
            
        }
        
      
    }
    
  
    
    
    
}