@istest
public class ProjectDataFromAvazaTest {
@istest
    static void mockTest()
    {  
        Projects__c projects = new Projects__c();
        projects.Name='Application';
        projects.Avaza_Project_ID__c=3455;
        insert projects;
       
        Test.setMock(HttpCalloutMock.class, new ProjectDataFromAvazaMock());
       
        Test.startTest();
        ProjectDataFromAvazaSchedular cli = new ProjectDataFromAvazaSchedular();
        String sch='0 26 19 7 5 ?';
        String jobId =system.schedule('Test',sch,cli);
        Test.stopTest();
 
       
    }
   
}