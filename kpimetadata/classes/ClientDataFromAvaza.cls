public class ClientDataFromAvaza {
    @future(callout=true)
    public static void getData()
   {
       String result;
       //String accessToken=system.Label.accessToken;
       String endpoint = 'https://api.avaza.com/api/Company?pageSize=1000';
       Http p = new Http();
       HttpRequest request = new HttpRequest();
       request.setEndpoint(endpoint);
       request.setMethod('GET');
       request.setHeader('Authorization','Bearer '+ System.Label.Avaza_Bearer_Token);
       HttpResponse response =p.send(request);
       result=response.getBody();
       Map<String, Object> totalList = (Map<String, Object>)JSON.deserializeUntyped(result);
       List<Object> companies = (List<Object>) totalList.get('Companies');
       List<Account> accLst = new List<Account>();
       List<Account> accountlst=[Select Name,BillingStreet,BillingState,BillingCity,BillingCountry,BillingPostalCode,Fax,Phone,Avaza_Company_ID__c
                                             from Account];
       List<Decimal> lst = new List<Decimal>();
       for(Account lap : accountlst)
       {
           lst.add(lap.Avaza_Company_ID__c);
       }
       for(Object obj : companies)
       {
           if(obj!=null)
           {
               Map<String, object> Companyvalues = (Map<String, Object>)obj;
               if(!lst.contains((Decimal)Companyvalues.get('CompanyID')))
               {   
                   Account acc = new Account();
                   acc.Avaza_Company_ID__c=(Decimal)Companyvalues.get('CompanyID');
                   acc.Name=(String)Companyvalues.get('CompanyName');
                   acc.BillingStreet=(String)Companyvalues.get('BillingAddressLine');
                   acc.BillingCity=(String)Companyvalues.get('BillingAddressCity');
                   acc.BillingState=(String)Companyvalues.get('BillingAddressState');
                   acc.BillingCountry=(String)Companyvalues.get('BillingCountryCode');
                   acc.BillingPostalCode=(String)Companyvalues.get('BillingAddressPostCode');
                   acc.Fax=(String)Companyvalues.get('Fax');
                   acc.Phone=(String)Companyvalues.get('Phone');
                   acc.Website=(String)Companyvalues.get('website');
                   accLst.add(acc);
               }
               else if(lst.contains((Decimal)Companyvalues.get('CompanyID')))
               {
                      for(Account a : accountlst)
                   {
                       if(a.Avaza_Company_ID__c==(Decimal)Companyvalues.get('CompanyID'))
                       {
                           a.Name=(String)Companyvalues.get('CompanyName');
                           a.BillingStreet=(String)Companyvalues.get('BillingAddressLine');
                           a.BillingState=(String)Companyvalues.get('BillingAddressState');
                           a.BillingCity=(String)Companyvalues.get('BillingAddressCity');
                           a.BillingCountry=(String)Companyvalues.get('BillingCountryCode');
                           a.BillingPostalCode=(String)Companyvalues.get('BillingAddressPostCode');
                           a.Fax=(String)Companyvalues.get('Fax');
                           a.Phone=(String)Companyvalues.get('Phone');
                           a.Website=(String)Companyvalues.get('website');
                           accLst.add(a);
                       }    
                   }
               }
           }   
       }
       upsert accLst;
   }
}