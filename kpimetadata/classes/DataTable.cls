global class DataTable {
    @AuraEnabled
public static List<Projects__c> getProjectDataFromAvaza()
    {
        // Aync Call.. Fire and Forget 
        ProjectDataFromAvaza.getProjectDataFromAvaza();
        
        return [Select Name,Avaza_Project_ID__c from Projects__c];
    }
}