public class WorkingDayCal {
 public static void WorkingDayCalculation(List<Working_Day__c> works)
     {
          String Location;
          Integer thismonth;
          String thisyear;
       List<Working_Day__c> w=new List<Working_Day__c>();
      
        
     for(Working_Day__c wd:works)
     {
     
       Location=wd.Location__c;
       thismonth=Integer.valueOf(wd.Month__c);
       thisyear=wd.Year__c;
    }
         Integer count=[SELECT COUNT() from Working_Day__c where Year__c=:thisyear];
    if(count==1)
    {
        for(Integer i=thismonth;i<=12;i++)
       {
             Integer days;
               Working_Day__c work=new Working_Day__c();
               work.Month__c=decimal.valueOf(i);
             work.Year__c=thisyear;
               work.Location__c='India';
             days=date.daysInMonth(integer.valueOf(thisyear),i);
             work.Total_Working_Days__c=BusinessDayCalculation.calculateWorkingDays(Date.newInstance(integer.valueOf(thisyear),i,1),Date.newInstance(integer.valueOf(thisyear),i,days),'India');
              w.add(work);
       }
       
        for(Integer i=thismonth;i<=12;i++)
       {
             Integer days;
               Working_Day__c work=new Working_Day__c();
               work.Month__c=decimal.valueOf(i);
             work.Year__c=thisyear;
               work.Location__c='Canada';
             days=date.daysInMonth(integer.valueOf(thisyear),i);
             work.Total_Working_Days__c=BusinessDayCalculation.calculateWorkingDays(Date.newInstance(integer.valueOf(thisyear),i,1),Date.newInstance(integer.valueOf(thisyear),i,days),'Canada');
              w.add(work);
       }
       
        for(Integer i=thismonth;i<=12;i++)
       {
             Integer days;
               Working_Day__c work=new Working_Day__c();
               work.Month__c=decimal.valueOf(i);
             work.Year__c=thisyear;
               work.Location__c='United States';
             days=date.daysInMonth(integer.valueOf(thisyear),i);
             work.Total_Working_Days__c=BusinessDayCalculation.calculateWorkingDays(Date.newInstance(integer.valueOf(thisyear),i,1),Date.newInstance(integer.valueOf(thisyear),i,days),'United States');
              w.add(work);
       }
       
        for(Integer i=thismonth;i<=12;i++)
       {
             Integer days;
               Working_Day__c work=new Working_Day__c();
               work.Month__c=decimal.valueOf(i);
             work.Year__c=thisyear;
               work.Location__c='Singapore';
             days=date.daysInMonth(integer.valueOf(thisyear),i);
             work.Total_Working_Days__c=BusinessDayCalculation.calculateWorkingDays(Date.newInstance(integer.valueOf(thisyear),i,1),Date.newInstance(integer.valueOf(thisyear),i,days),'Singapore');
              w.add(work);
       }
        insert w;
       }
      
     }
}