@isTest
public class TestBusinessDayCalculation {
    @isTest
    public static void calculateWorkingDays(){
        Date startDate = System.Date.newInstance(2020, 01, 09); 
        Date endDate = System.Date.newInstance(2020, 09, 09);
        String Location = 'Canada';
        Integer workingDays = 0;
        Holiday_calendar__c holidayDate=new Holiday_Calendar__c(Holiday_Date__c =System.Date.newInstance(2020, 08, 25), Location__c = 'Canada');
        Test.startTest();
        insert holidayDate;
        set<Date> holidaysSet = new set<Date>(); 
        for(Holiday_Calendar__c currHoliday : [Select Holiday_Date__c from Holiday_Calendar__c where Location__c includes (:Location)]){
            holidaysSet.add(currHoliday.Holiday_Date__c);  
        }   
        for(integer i=0; i <= startDate.daysBetween(endDate); i++){
            Date dt = startDate + i;  
            DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());  
            String todayDay = currDate.format('EEEE');  
            if(todayDay != 'Saturday' && todayDay !='Sunday' && (!holidaysSet.contains(dt))){
                workingDays = workingDays + 1;  
            }     
        }
        Test.stopTest();
        System.assert(System.equals(workingdays,170));
         
    }

}