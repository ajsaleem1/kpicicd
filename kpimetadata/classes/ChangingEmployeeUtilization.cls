public class ChangingEmployeeUtilization {
    
    @Invocablevariable(required = true) public sObject lastDate;
    @Invocablevariable(required = true) public string empId;
    
    @InvocableMethod
    public static void ChangeTargetUtilization(List<sObject> variableList){
        
        String id = String.valueOf(variableList.get(1));
        for(KPI_Employee_Target_Utilization__c var : [select id, Target_Utilization_Percentage__c from KPI_Employee_Target_Utilization__c where Employee_Name__r.id = :id]){
            
            System.debug(var);
        }
        
    }

}