@isTest
public class ProjectAssociateTriggerTest {
    
    @isTest
    Static void testProject(){
        
        Employee__c emp = new Employee__c(name = 'test', email__c = 'test@gmail.com', phone__c = '1234567890');
        insert emp;
        Account cust = new Account(name = 'test customer');
        insert cust;
        System.debug(cust);
        projects__c project = new Projects__c(customer__c = cust.id, name = 'test project');
        insert project;
        Project_Associate__c projectAssociate = new Project_Associate__c(employee__c = emp.id, project__c = project.id);
        insert projectAssociate;
        
        Project_Associate__c projectAssociateDup = new Project_Associate__c(employee__c = emp.id, project__c = project.id);
        
        
        
        Test.startTest();
        try{
            insert projectAssociateDup;
        }catch(DmlException e){
            System.assert(true);
        }
        
            
            
    }

}