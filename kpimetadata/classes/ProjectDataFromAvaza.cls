public class ProjectDataFromAvaza {
    @future(callout=true)
public static void getProjectDataFromAvaza()
    {
       String result;
       //String AuthToken=system.Label.AuthToken;
       String endpoint = 'https://api.avaza.com/api/Project?pageSize=1000';
       Http p = new Http();
       HttpRequest request = new HttpRequest();
       request.setEndpoint(endpoint);
       request.setMethod('GET');
       request.setHeader('Authorization','Bearer '+ System.Label.Avaza_Bearer_Token);
       HttpResponse response =p.send(request);
       result=response.getBody();
       List<Projects__c> projectList = new List<Projects__c>();
       List<Projects__c> listOfProject =[Select Name, Avaza_Project_ID__c from Projects__c];
       List<Decimal> listOfIds = new List<Decimal>();
       for(Projects__c ids : listOfProject)
       {
           listOfIds.add(ids.Avaza_Project_ID__c);
       }
       Map<String, Object> totalList = (Map<String, Object>)JSON.deserializeUntyped(result);
       List<Object> projects = (List<Object>) totalList.get('Projects');
       for(Object obj : projects)
       {
           if(obj!=null)
           {
               Map<String, object> Projectvalues = (Map<String, Object>)obj;
               if(!listOfIds.contains((Decimal)Projectvalues.get('ProjectID')))
               {
                   Projects__c pr = new Projects__c();
                   pr.Name=(String)Projectvalues.get('Title');
                   pr.Avaza_Project_ID__c=(Decimal)Projectvalues.get('ProjectID');
                   projectList.add(pr);
               }
               else if(listOfIds.contains((Decimal)Projectvalues.get('ProjectID')))
{
                   for(Projects__c  pro : listOfProject)
                   {   if(pro.Avaza_Project_ID__c==(Decimal)Projectvalues.get('ProjectID'))
                    {
                            pro.Name=(String)Projectvalues.get('Title');
                        projectList.add(pro);
                        }
                   }
                }                
           }
       }
       upsert projectList;
    }
}