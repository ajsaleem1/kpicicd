public class HolidayNotification {
    public static void insertHoliday(List<Holiday_Calendar__c> ho){
        String location;
        System.debug(ho);
        for(Holiday_Calendar__c h: ho){
        	location=h.Location__c;
    	}
    	List<Employee__c> e=[Select Email__c,location__c from Employee__c where location__c=:location];
      	List<String> toadd=new List<String>();
     	for(Employee__c em:e){
            toadd.add(em.Email__c);
     	}
     	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    	mail.setToAddresses(toadd);
       	mail.setPlainTextBody('<h1>Dear employee,</h1><br/><br/>');
    	mail.setSubject('Regarding Holiday Insert');
   		String body='<h2>Dear Employee,</h2><br/> we have inserted a record in the holiday calendar in ';
    	body=body+Location;
    	mail.setHtmlBody(body);
    	mail.setSenderDisplayName('Ajay');
    	Messaging.Email[] emails =new Messaging.Email[]{mail};
        Messaging.sendEmail(emails);
   	}
    public static void updateHoliday(List<Holiday_Calendar__c> ho){
        String location;
        System.debug(ho);
        for(Holiday_Calendar__c h: ho){
        	location=h.Location__c;
    	}
    	List<Employee__c> e=[Select Email__c,location__c from Employee__c where location__c=:location];
      	List<String> toadd=new List<String>();
     	for(Employee__c em:e){
            toadd.add(em.Email__c);
     	}
     	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    	mail.setToAddresses(toadd);
       	mail.setPlainTextBody('<h1>Dear employee,</h1><br/><br/>');
    	mail.setSubject('Regarding Holiday Update');
   		String body='<h2>Dear Employee,</h2><br/> we have updated a record from the holiday calendar in ';
    	body=body+Location;
    	mail.setHtmlBody(body);
    	mail.setSenderDisplayName('Ajay');
    	Messaging.Email[] emails =new Messaging.Email[]{mail};
        Messaging.sendEmail(emails);
   	}
	public static void deleteHoliday(List<Holiday_Calendar__c> ho){
        String location;
        System.debug(ho);
        for(Holiday_Calendar__c h: ho){
        	location=h.Location__c;
    	}
    	List<Employee__c> e=[Select Email__c,location__c from Employee__c where location__c=:location];
      	List<String> toadd=new List<String>();
     	for(Employee__c em:e){
            toadd.add(em.Email__c);
     	}
     	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    	mail.setToAddresses(toadd);
       	mail.setPlainTextBody('<h1>Dear employee,</h1><br/><br/>');
    	mail.setSubject('Regarding deletion');
   		String body='<h2>Dear Employee,</h2><br/> we have deleted the record from the holiday calendar in ';
    	body=body+Location;
    	mail.setHtmlBody(body);
    	mail.setSenderDisplayName('Ajay');
    	Messaging.Email[] emails =new Messaging.Email[]{mail};
        Messaging.sendEmail(emails);
   	}
}