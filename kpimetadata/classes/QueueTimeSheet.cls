public class QueueTimeSheet implements Queueable, Database.AllowsCallouts  {
    
    public String updatedAfter = null;
    public String startDate = null;
    
    public QueueTimeSheet(String updatedAfter, String StartDate){
        
        this.updatedAfter = updatedAfter;
        this.StartDate = StartDate;
    }
    
    public void execute(QueueableContext context) {
        
        TimeSheetFiller.fillTimeSheet(updatedAfter, StartDate);
    }
    
    @AuraEnabled
    public static void fillTimeSheet(String updatedAfter, String StartDate){
        
       System.enqueueJob(new QueueTimeSheet(updatedAfter, StartDate));
        
    }
    
    

}