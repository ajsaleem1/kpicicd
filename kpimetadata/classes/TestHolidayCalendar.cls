@isTest
public class TestHolidayCalendar{
	@isTest 
    static void testHolidayCalendarDuplicatesInsertSameDateTrigger(){
        Holiday_calendar__c holidayDate=new Holiday_Calendar__c(Holiday_Date__c = System.date.today());
        insert holidayDate;
        Holiday_calendar__c sameHolidaydate=new Holiday_Calendar__c(Holiday_Date__c = System.date.today());
        Test.startTest();
        Database.SaveResult result = Database.insert(sameHolidaydate,false);
        Test.stopTest();
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('Cannot insert',
                                 result.getErrors()[0].getMessage());
    }
    
    @isTest
    static void testHolidayCalendarDuplicatesInsertDiffLocationSameDateTrigger(){
        Holiday_calendar__c holidayDate=new Holiday_Calendar__c(Holiday_Date__c =System.date.today(), Location__c = 'India');
        insert holidayDate;
        Holiday_calendar__c sameHolidaydate=new Holiday_Calendar__c(Holiday_Date__c =System.date.today(), Location__c = 'India');
        Test.startTest();
        Database.SaveResult result = Database.insert(sameHolidaydate,false);
        Test.stopTest();
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('cannot insert',
                             result.getErrors()[0].getMessage());
    }
    @isTest
    static void testHolidayCalendarDuplicatesInsertSameLocationDiffDateTrigger(){
        Holiday_calendar__c holidayDate=new Holiday_Calendar__c(Holiday_Date__c =System.date.today(), Location__c = 'India');
        insert holidayDate;
        Holiday_calendar__c sameHolidaydate=new Holiday_Calendar__c(Holiday_Date__c =System.date.today(), Location__c = 'USA');
        Test.startTest();
        Database.SaveResult result = Database.insert(sameHolidaydate,false);
        Test.stopTest();
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('cannot insert',
                             result.getErrors()[0].getMessage());
    }
    @isTest
    static void testHolidayCalendarDuplicatesUpdateSameLocationDiffDateTrigger(){
        Holiday_calendar__c holidayDate=new Holiday_Calendar__c(Holiday_Date__c =System.date.today(), Location__c = 'India');
        insert holidayDate;
        Holiday_calendar__c sameHolidaydate=new Holiday_Calendar__c(Holiday_Date__c =System.Date.newInstance(2020, 12, 25), Location__c = 'India');
        Test.startTest();
        Database.SaveResult result = Database.update(sameHolidaydate,false);
        Test.stopTest();
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('cannot update',
                             result.getErrors()[0].getMessage());
    }
    @isTest
    static void testHolidayCalendarDuplicatesUpdateDiffLocationSameDateTrigger(){
        Holiday_calendar__c holidayDate=new Holiday_Calendar__c(Holiday_Date__c =System.Date.newInstance(2020, 12, 25), Location__c = 'Canada');
        insert holidayDate;
        Holiday_calendar__c sameHolidaydate=new Holiday_Calendar__c(Holiday_Date__c =System.Date.newInstance(2020, 12, 25), Location__c = 'India');
        Test.startTest();
        Database.SaveResult result = Database.update(sameHolidaydate,false);
        Test.stopTest();
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('cannot update',
                             result.getErrors()[0].getMessage());
    }
}