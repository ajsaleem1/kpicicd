public class CalenderClass {
    @AuraEnabled
    public static List<integer> getMethod(String year,String month,String location)
    {
        integer holidayCount=0;
        integer totalWorkingDays=0;
        List<String> str=new List<String>();
        List<Integer> num=new List<Integer>();
        List<Holiday_Calendar__c> holiday = [Select Holiday_Date__c,Location__c from Holiday_Calendar__c];
        List<Working_Day__c> working=[Select Year__c,Total_Working_Days__c,Location__c,Month__c from Working_Day__c where
                                      Year__c=:year and
                                      Month__c=:Decimal.valueOf(month) and
                                      Location__c=:location];
        for(Holiday_Calendar__c holi:holiday)
        {
            str=holi.Location__c.split(';');
            if(holi.Holiday_Date__c.year()==integer.valueOf(year) && holi.Holiday_Date__c.month()==integer.valueOf(month) && str.contains(location))
            {
                holidayCount=holidayCount+1;
               
            }
            str.clear();
        }
        system.debug(holidayCount);
        num.add(holidayCount);
        for(Working_Day__c work:working)
        {
            totalWorkingDays=integer.valueOf(work.Total_Working_Days__c);
        }
       system.debug(totalWorkingDays);
        num.add(totalWorkingDays);
        return num;
    }
}